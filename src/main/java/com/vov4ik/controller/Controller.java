package com.vov4ik.controller;

import com.vov4ik.model.BlockingQueueTask;
import com.vov4ik.model.LockSynchronization;

public class Controller {

    public void showLockSynchronization() {
        LockSynchronization lockSynchronization = new LockSynchronization();
        lockSynchronization.showLockSynchronization();
    }

    public void showBlockingQueue() {
        BlockingQueueTask blockingQueueTask = new BlockingQueueTask();
        blockingQueueTask.showBlockingQueue();
    }
}
