
package com.vov4ik.view;


import com.vov4ik.controller.Controller;
import com.vov4ik.view.logger.MyLogger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private MyLogger logger;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scanner;

    public MyView() {
        logger = new MyLogger();
        controller = new Controller();
        scanner = new Scanner(System.in);
        setMenu();
        setMethodsMenu();
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "Show Lock Synchronization");
        menu.put("2", "Show Blocking Queue");
        menu.put("Q", "Quit");
    }

    private void setMethodsMenu() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showLockSynchronization);
        methodsMenu.put("2", this::showBlockingQueueWork);
    }

    private void showLockSynchronization() {
        controller.showLockSynchronization();
    }

    private void showBlockingQueueWork() {
        controller.showBlockingQueue();
    }

    public void showMenu() {
        String input;
        do {
            logger.printInfo("Menu:\n");
            for (String key : menu.keySet()) {
                logger.printInfo(key + " - " + menu.get(key));
            }
            logger.printInfo("Please, select one of points");
            input = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(input).print();
            } catch (Exception e) {
                logger.printWarning("Your input was incorrect. \nPlease, try again");
            }
        } while (!input.equals("Q"));
    }
}


