package com.vov4ik.model;

import com.vov4ik.view.logger.MyLogger;

import java.util.concurrent.locks.ReentrantLock;

public class LockSynchronization {
    private MyLogger logger = new MyLogger();
    private ReentrantLock lock = new ReentrantLock();
    private int counter = 0;

    private void first() {
        try {
            lock.lock();
            for (int i = 0; i < 10; i++) {
                counter++;
                logger.printInfo("First method, execution " + i
                        + "\ncounter = " + counter);
            }
        } finally {
            lock.unlock();
        }
    }

    private void second() {
        try {
            lock.lock();
            for (int i = 0; i < 10; i++) {
                counter++;
                logger.printInfo("Second method, execution " + i
                        + "\ncounter = " + counter);
            }
        } finally {
            lock.unlock();
        }
    }

    private void third() {
        try {
            lock.lock();
            for (int i = 0; i < 10; i++) {
                counter++;
                logger.printInfo("Third method, execution " + i
                        + "\ncounter = " + counter);
            }
        } finally {
            lock.unlock();
        }
    }

    public void showLockSynchronization() {
        Thread t1 = new Thread(this::first);
        Thread t2 = new Thread(this::second);
        Thread t3 = new Thread(this::third);
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            logger.printError("Thread was interrupted");
            e.printStackTrace();
        }
    }
}
