package com.vov4ik.model;

import com.vov4ik.view.logger.MyLogger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class BlockingQueueTask {
    private MyLogger logger;
    private BlockingQueue<Integer> blockingQueue;

    public BlockingQueueTask() {
        logger = new MyLogger();
        blockingQueue = new PriorityBlockingQueue<>();
    }

    private Thread sender = new Thread(() -> {
        for (int i = 0; i < 15; i++) {
            try {
                blockingQueue.put(i);
                logger.printInfo("" + i + "was inputted into queue");
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        logger.printInfo("all int's were inputted");
    });

    private Thread receiver = new Thread(() -> {
        while (true) {
            try {
                if (!(blockingQueue.take() < 15)) break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
                logger.printInfo("" + blockingQueue.take() + "was outputted from queue");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        logger.printInfo("all int's were outputted");
    });

    public void showBlockingQueue() {
        sender.start();
        receiver.start();
        try {
            sender.join();
            receiver.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
